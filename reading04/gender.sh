#!/bin/sh

# gender.sh

URL=https://www3.nd.edu/~pbui/teaching/cse.20289.sp17/static/csv/demographics.csv

count_gender() {
    column=	# TODO: Determine appropriate column from first argument
    gender=	# TODO:	Gender is second argument
    # TODO extract gender data for specified year and group
}

for year in $(seq 2013 2019); do
    echo $year $(count_gender $year F) $(count_gender $year M)
done
